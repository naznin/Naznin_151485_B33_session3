

<?php
//boolean type
$x=true;
$y=false;
?>


<?php
//integer type
echo $a=123;//decimal type
echo $a=-123;//negetive integer
echo $a=0123;//octal number (equivalent to 83 decimal)
echo $a=0x1A;//hexadecimal number(equivalent to 26 decimal)
echo $a=11111111;//binary(equivalent to 255 decimal)
?>

<?php
//float type
echo $a=1.234;
echo $b=1.2e3;
?>

<?php
$color = array("black","white","red","green");
echo "my favourite color is ".$color[0].", ".$color[1].", ".$color[2].", ".$color[3].".";
?>

<?php
//string type
echo $str='this is single quoted string .';
echo $str=" this is double quoted string .";
echo $str=<<<EOD
example of string spanning
multiple lines using
heredoc syntax
EOD;
echo $str=<<<'EOD'
example of string spanning
multiple lines using
newdoc syntax
EOD;

?>


