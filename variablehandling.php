

<?php
//floatval()
$string_with_float="235.269bitm45";
echo floatval($string_with_float);
?>
<?php
//empty
echo "</br>";
$var1=2;
$var2='';
var_dump ( empty($var2));
echo "</br>";
var_dump ( empty($var1));
?>

<?php
//testing is_array
$int_var=2;
$array_val=[1,2,3];
var_dump(is_array($int_var));
var_dump(is_array($array_val));
?>


<?php
// testing is_null
$var_1=2;
$var_2='';
$var_3=NULL;
echo "</br>";
var_dump(is_null($var_1));
echo "</br>";
var_dump(is_null($var_2));
echo "</br>";
var_dump(is_null($var_3));
?>

<?php
//isset and unset testing
$var='';
$a='abcd';
$b='efgh';
$c=NULL;
var_dump(isset($var));// returns bool(true)
var_dump(isset($a));// returns bool(true)
var_dump(isset($b));// returns bool(true)
var_dump(isset($c));// returns bool(false)
unset($b);//unset variable b
var_dump(isset($b));// returns bool(false)
?>

<?php
//print_r
echo "</br>";
$var1='abc';
$var2=123.33;
echo "$var1";//shows value
print_r($var1);//returns value
print_r($var1);//returns value
echo "$var2";//shows value
$abc= array('sub1'=>'math','sub2'=>'physics');
echo "$abc";//shows warning coz echo cant show array
print_r($abc);//returns the array
?>

<?php
//serialize()
echo "</br>";
$serialize_value=serialize(array('math','physics','chemistry'));//serialize or convert the array to string
echo $serialize_value;//return string which was converted from array
echo"</br>";
//unserialize()
$unserialize_value=unserialize($serialize_value);
echo $unserialize_value;//shows warning
print_r($unserialize_value);//return array
?>

<?php
//gettype testing
echo "</br>";
echo gettype(102)."</br>";//shows integer
echo gettype(102.33)."</br>";//shows float
echo gettype(" ")."</br>";//shows empty
echo gettype(NULL)."</br>";//shows NULL
echo gettype(array())."</br>";//shows array
echo gettype(new stdclass())."</br>";//shows object
?>

<?php
